﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise1
{
    internal class Book
    {
        public int Isbn { get; set; }
        public string? BookName { get; set; }
        public string? AuthorName { get; set; }
        public string? PublisherName { get; set; }

        public Book()
        {
        }

        public Book(int isbn, string? bookName, string? authorName, string? publisherName)
        {
            this.Isbn = isbn;
            this.BookName = bookName;

            this.AuthorName = authorName;
            this.PublisherName = publisherName;
        }

        public string GetBookInformation()
        {
            return string.Format("{0, -15}{1, -15}{2, -15}{3, -15}", this.Isbn, this.BookName, this.AuthorName, this.PublisherName);
        }

    }
}