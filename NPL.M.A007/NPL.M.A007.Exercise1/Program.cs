﻿// See https://aka.ms/new-console-template for more information


using NPL.M.A007.Exercise1;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
Console.InputEncoding = Encoding.Unicode;

Console.WriteLine(string.Format("{0, -15}{1, -15}{2, -15}{3, -15}","Isbn", "Book Name", "Author Name","Publisher Name"));
Book book = new Book(123456789, "Harry Potter", "J.K.Rowling", "Kim Dong");
Console.WriteLine(book.GetBookInformation());
