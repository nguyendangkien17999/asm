﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class MyOwnAutoShop
    {
        Sedan sedan = new Sedan(221, 3123, "Red", 182);
        Ford ford1 = new Ford(156, 2147, "Black", 2021, 262);
        Ford ford2 = new Ford(156, 2147, "Blue", 2019, 182);
        Truck truck1 = new Truck(180, 2350, "Blue", 262);
        Truck truck2 = new Truck(200, 2360, "Red", 262);

        public void DisplayPrice()
        {
            sedan.DisplayPrice();
            ford1.DisplayPrice();
            ford2.DisplayPrice();
            truck1.DisplayPrice();
            truck2.DisplayPrice();
        }
    }
}
