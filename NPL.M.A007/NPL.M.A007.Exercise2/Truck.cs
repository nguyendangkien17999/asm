﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Truck : Car
    {
        public int Weight;

        public Truck()
        {

        }

        public Truck(decimal speed, double regularPrice, string color, int weight) : base(speed, regularPrice, color)
        {
            this.Weight = weight;
        }

        public override double GetSalePrice()
        {
            if (Weight > 2000)
            {
                return this.RegularPrice * 0.9;
            }
            else
            {
                return this.RegularPrice * 0.8;
            }
        }
        public void DisplayPrice()
        {
            Console.WriteLine(Color + " " + "Truck Price: " + this.GetSalePrice());
        }
    }
}
