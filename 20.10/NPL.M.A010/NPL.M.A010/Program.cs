﻿using NPL.M.A010;

Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;
int luaChon;
Service service = new Service();
do
{
    Console.WriteLine("-----Assignment 11- Ooutlook Emulator-----");
    Console.WriteLine("Please select");
    Console.WriteLine("1. New Mail");
    Console.WriteLine("2. Sent");
    Console.WriteLine("3. Draft");
    Console.WriteLine("4. ReadFile");
    Console.WriteLine("5.Exit");
    Console.Write("Enter Menu Option Number: ");
    luaChon = int.Parse(Console.ReadLine());

    switch (luaChon)
    {
        case 1:
            service.NewMail();
            break;
        case 2:
            service.Sent();
            break;
        case 3:
            service.Draft();
            break;
        case 4:
            service.ReadFile();
            break;
        case 0:
            Console.WriteLine("Kết thúc chương trình!");
            break;
        default:
            Console.WriteLine("Không có lựa chọn!");
            break;
    }
}
while (luaChon > 0 && luaChon <= 4);
