﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class LanguageEmployee
    {
        public string LanguageName { get; set; }
        public int EmloyeeId { get; set; }

        public LanguageEmployee() { }

        public LanguageEmployee(string languageName, int emloyeeId)
        {
            LanguageName = languageName;
            EmloyeeId = emloyeeId;
        }
    }
}
