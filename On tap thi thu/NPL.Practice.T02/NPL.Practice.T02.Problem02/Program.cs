﻿// See https://aka.ms/new-console-template for more information



Console.WriteLine("Enter the number of elements in the array: ");
int n = int.Parse(Console.ReadLine());

int[] inputArray = new int[n];

Console.WriteLine("Enter the elements of the array: ");
for (int i = 0; i < n; i++)
{
    inputArray[i] = int.Parse(Console.ReadLine());
}

Console.WriteLine("Enter the subarray length:");
int subLength = int.Parse(Console.ReadLine());

int maxSum = FindMaxSubArray(inputArray, subLength);

Console.WriteLine("The maximum contiguous subarray sum is: " + maxSum);

static  int FindMaxSubArray(int[] inputArray, int subLength)
{
    int maxSum = int.MinValue;

    // Kiểm tra xem SubLength có hợp lệ không
    if (subLength <= 0 || subLength > inputArray.Length)
    {
        throw new ArgumentException("Invalid subLength");
    }

    // inputArray và tìm tổng tối đa
    for (int i = 0; i <= inputArray.Length - subLength; i++)
    {
        int currentSum = 0;

        // Tính tổng của mảng con hiện tại
        for (int j = i; j < i + subLength; j++)
        {
            currentSum += inputArray[j];
        }

        // Cập nhật maxSum nếu tổng hiện tại lớn hơn
        if (currentSum > maxSum)
        {
            maxSum = currentSum;
        }
    }

    return maxSum;
}


