﻿// See https://aka.ms/new-console-template for more information

Console.WriteLine("Content: ");
string contentOfArticle = Console.ReadLine();
Console.WriteLine("Max length: ");
int maxLength = int.Parse(Console.ReadLine());

string a = GetArticleSummary(contentOfArticle, maxLength);
Console.WriteLine("Summary: "+ a);
static string GetArticleSummary(string contentOfArticle, int maxLength)
{
    if(contentOfArticle.Length <= maxLength)
    {
        return contentOfArticle.Trim();
    }

    if (contentOfArticle[maxLength-1] == ' ')
    {
        return contentOfArticle.Substring(0,maxLength-1).Trim()+"...";
    }
    if (contentOfArticle[maxLength-1]!= ' '&& contentOfArticle[maxLength]==' ')
    {
        return contentOfArticle.Substring(0, maxLength).Trim()+"...";
    }
    if (contentOfArticle[maxLength-1] != ' '&& contentOfArticle[maxLength]!= ' ')
    {
        for(int i = maxLength-1 ;i >= 0;i--) 
        {
            if (contentOfArticle[i] == ' ')
            {
                return contentOfArticle.Substring(0, i).Trim() + "...";
            }
        }
    }

    return "...";
}
